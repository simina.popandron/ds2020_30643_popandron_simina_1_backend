
CREATE SEQUENCE PATIENT_SEQUENCE START WITH 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS PATIENT (

    ID int NOT NULL PRIMARY KEY,
    NAME varchar(30),
    BIRTHDATE timestamp,
    GENDER varchar(20),
    ADDRESS varchar(50),
    MEDICALRECORD varchar(200),
    USER_ID int NOT NULL,
    CAREGIVER_ID int NOT NULL
);
ALTER TABLE PATIENT ADD FOREIGN KEY (USER_ID) REFERENCES USER(ID);
ALTER TABLE PATIENT ADD FOREIGN KEY (CAREGIVER_ID) REFERENCES CAREGIVER(ID);
