insert into USER values(nextval('PUBLIC', 'USER_SEQUENCE'), 'simina.popandron', 'simina', 'doctor');
insert into USER values(nextval('PUBLIC', 'USER_SEQUENCE'), 'iris.bara', 'iris', 'caregiver');
insert into USER values(nextval('PUBLIC', 'USER_SEQUENCE'), 'cristina.popescu', 'cristina', 'patient');
insert into USER values(nextval('PUBLIC', 'USER_SEQUENCE'), 'natalia.roncea', 'natalia', 'patient');
insert into USER values(nextval('PUBLIC', 'USER_SEQUENCE'), 'mando.kid', 'mando', 'caregiver');
insert into USER values(nextval('PUBLIC', 'USER_SEQUENCE'), 'adina.popandron', 'adina', 'patient');