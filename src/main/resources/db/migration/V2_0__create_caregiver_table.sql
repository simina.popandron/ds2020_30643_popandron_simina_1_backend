
CREATE SEQUENCE CAREGIVER_SEQUENCE START WITH 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS CAREGIVER (
--defined by ID, name, birth date, gender, address, list of patients taken care of
    ID int NOT NULL PRIMARY KEY,
    NAME varchar(30),
    BIRTHDATE timestamp,
    GENDER varchar(20),
    ADDRESS varchar(50),
    USER_ID int NOT NULL
);
ALTER TABLE CAREGIVER ADD FOREIGN KEY (USER_ID) REFERENCES USER(ID);