package ro.tuc.ds2020.controllers;

import ro.tuc.ds2020.services.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/intake")
public class IntakeController {

    private final IntakeService intakeService;

    @Autowired
    public IntakeController(IntakeService intakeService) {
        this.intakeService = intakeService;
    }

    @PostMapping()
    public Integer insertPatientDTO() {
        return null;
    }

}
