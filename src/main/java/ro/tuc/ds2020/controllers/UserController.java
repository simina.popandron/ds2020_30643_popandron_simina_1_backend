package ro.tuc.ds2020.controllers;

import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/login")
@CrossOrigin()
public class UserController {
    @Autowired
    private UserService userService;


    @PostMapping()
    public UserDTO login(@RequestBody UserDTO userDTO) {
        return userService.logIn(userDTO.getUsername(), userDTO.getPassword());
    }

    @GetMapping(value = "/{id}")
    public UserDTO findById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }
}
