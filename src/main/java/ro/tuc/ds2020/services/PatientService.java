package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.UserRepository;
import ro.tuc.ds2020.dtos.validators.PatientValidator;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;
    private final UserRepository userRepository;
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, UserRepository userRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.userRepository = userRepository;
        this.caregiverRepository = caregiverRepository;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    public List<PatientDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();
        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public void delete(Integer id){
        this.patientRepository.deleteById(id);
        LOGGER.error("Patient with id {} was deleted", id);
    }


    public Integer insert(PatientDTO patientDTO) {
        PatientValidator.validateInsertOrUpdate(patientDTO);
        Patient patient = PatientBuilder.generateEntityFromDTO(patientDTO);
        User user = new User(patientDTO.getUsername(), patientDTO.getPassword(), "patient");
        Optional<Caregiver> caregiver = caregiverRepository.findById(patientDTO.getIdCaregiver());
        patient.setCaregiver(caregiver.get());
        patient.setUser(user);
        userRepository.save(user);
        LOGGER.error("Patient with id {} was not found in db", patient.getId());
        return patientRepository.save(patient).getId();
    }

    public Integer update(PatientDTO patientDTO) {
        PatientValidator.validateInsertOrUpdate(patientDTO);
        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());
        Optional<Caregiver> caregiver = caregiverRepository.findById(patientDTO.getIdCaregiver());
        User user = patient.get().getUser();
        Patient patient_updated = PatientBuilder.generateEntityFromDTO(patientDTO);
        user.setUsername(patientDTO.getUsername());
        user.setPassword(patientDTO.getPassword());
        patient_updated.setUser(user);
        patient_updated.setCaregiver(caregiver.get());
        userRepository.save(user);
        LOGGER.debug("Patient with id {} was updated", patient_updated.getId());
        return patientRepository.save(patient_updated).getId();
    }

    public PatientDTO findPatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);
        if (!patient.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }
}


