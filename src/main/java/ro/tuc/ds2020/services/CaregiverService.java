package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.dtos.validators.CaregiverValidator;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, UserRepository userRepository) {
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);

    public List<CaregiverDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();

        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public void delete(Integer id){
        this.caregiverRepository.deleteById(id);
        LOGGER.error("Caregiver with id {} was deleted", id);

    }

    public Integer insert(CaregiverDTO caregiverDTO) {
        CaregiverValidator.validateInsertOrUpdate(caregiverDTO);
        Caregiver caregiver = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        User user = new User(caregiverDTO.getUsername(), caregiverDTO.getPassword(), "caregiver");
        caregiver.setUser(user);
        userRepository.save(user);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiverRepository.save(caregiver).getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {
        CaregiverValidator.validateInsertOrUpdate(caregiverDTO);
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());
        User user = caregiver.get().getUser();
        user.setUsername(caregiverDTO.getUsername());
        user.setPassword(caregiverDTO.getPassword());
        Caregiver caregiver_updated = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        caregiver_updated.setUser(user);
        userRepository.save(user);
        LOGGER.debug("Caregiver with id {} was updated", caregiver_updated.getId());
        return caregiverRepository.save(caregiver_updated).getId();
    }

    //not used
    public CaregiverDTO findCaregiverById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if (!caregiver.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }


    public List<PatientDTO> findAllPatients(Integer id) {
        Optional<Caregiver> caregiverId = caregiverRepository.getCaregiverByUserId(id);
        List<Patient> caregiverList = caregiverRepository.getAllPatients(caregiverId.get().getId());
        return caregiverList.stream().map(PatientBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

}
