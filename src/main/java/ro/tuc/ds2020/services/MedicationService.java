package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.dtos.validators.MedicationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    public List<MedicationDTO> findAll(){
        List<Medication> medications = medicationRepository.getAllOrdered();

        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public void delete(Integer id){
        this.medicationRepository.deleteById(id);
        LOGGER.error("Medication with id {} was deleted", id);

    }

    public Integer insert(MedicationDTO medicationDTO) {
        MedicationValidator.validateInsertOrUpdate(medicationDTO);
        LOGGER.error("Medication added to the db");
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public Integer update(MedicationDTO medicationDTO) {
        MedicationValidator.validateInsertOrUpdate(medicationDTO);
        LOGGER.error("Medication updated");
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public MedicationDTO findMedicationById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);
        if (!medication.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }
}
