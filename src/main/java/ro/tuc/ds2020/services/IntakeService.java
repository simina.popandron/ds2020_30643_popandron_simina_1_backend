package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.IntakeDTO;
import ro.tuc.ds2020.repositories.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IntakeService {

    private final IntakeRepository intakeRepository;
    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;

    @Autowired
    public IntakeService(IntakeRepository intakeRepository, PatientRepository patientRepository, MedicationRepository medicationRepository) {
        this.intakeRepository = intakeRepository;
        this.patientRepository = patientRepository;
        this.medicationRepository = medicationRepository;
    }

    public Integer delete(Integer id) {
        return null;
    }

    public Integer insert(IntakeDTO intakeDTO) {
            return null;
    }

    public Integer update(IntakeDTO intakeDTO) {
        return null;
    }

    public List<IntakeDTO> findAllIntakes(Integer id) {
        return null;
    }

}
