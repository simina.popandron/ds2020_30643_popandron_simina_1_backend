package ro.tuc.ds2020.repositories;

import ro.tuc.ds2020.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository <Patient, Integer>{
    Patient findByName(String name);

    @Query(value = "SELECT u FROM Patient u ORDER BY u.id")
    List<Patient> getAllOrdered();


}
