package ro.tuc.ds2020.repositories;

import ro.tuc.ds2020.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MedicationRepository extends JpaRepository<Medication, Integer> {

    Medication findByName(String name);

    @Query(value = "SELECT u FROM Medication u ORDER BY u.id")
    List<Medication> getAllOrdered();

}
