package ro.tuc.ds2020.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "CAREGIVER")
public class Caregiver {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=SEQUENCE, generator="caregiver_sequence_generator")
    @SequenceGenerator(name = "caregiver_sequence_generator", sequenceName = "CAREGIVER_SEQUENCE", allocationSize = 1)
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "BIRTHDATE")
    private Timestamp birthDate;
    @Column(name = "GENDER")
    private String gender;
    @Column(name = "ADDRESS")
    private String address;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID", unique = true, referencedColumnName = "ID")
    private User user;

    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY)
    private List<Patient> patients;


    public Caregiver() {
    }

    public Caregiver(Integer id, String name, Timestamp birthDate, String gender, String address) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }


}
