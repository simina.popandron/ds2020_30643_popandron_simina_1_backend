package ro.tuc.ds2020.entities;

import javax.persistence.*;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "MEDICATION")
public class Medication {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=SEQUENCE, generator="medication_sequence_generator")
    @SequenceGenerator(name = "medication_sequence_generator", sequenceName = "MEDICATION_SEQUENCE", allocationSize = 1)
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SIDEEFFECTS")
    private String sideEffects;
    @Column(name = "DOSAGE")
    private String dosage;

    public Medication() {
    }

    public Medication(Integer id, String name, String sideEffects, String dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

}
