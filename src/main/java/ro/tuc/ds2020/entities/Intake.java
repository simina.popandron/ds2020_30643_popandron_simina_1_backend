package ro.tuc.ds2020.entities;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "INTAKE")
public class Intake {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=SEQUENCE, generator="intake_sequence_generator")
    @SequenceGenerator(name = "intake_sequence_generator", sequenceName = "INTAKE_SEQUENCE", allocationSize = 1)
    private Integer id;
    @Column(name = "STARTDATE")
    private Timestamp startDate;
    @Column(name = "ENDDATE")
    private Timestamp endDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PATIENT_ID")
    private Patient patient;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "MEDICATION_ID")
    private Medication medication;

    public Intake() {
    }

    public Intake(Timestamp startDate, Timestamp endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

}
