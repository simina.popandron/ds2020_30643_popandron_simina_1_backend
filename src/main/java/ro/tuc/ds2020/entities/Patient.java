package ro.tuc.ds2020.entities;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "PATIENT")
public class Patient {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=SEQUENCE, generator="patient_sequence_generator")
    @SequenceGenerator(name = "patient_sequence_generator", sequenceName = "PATIENT_SEQUENCE", allocationSize = 1)
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "BIRTHDATE")
    private Timestamp birthDate;
    @Column(name = "GENDER")
    private String gender;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "MEDICALRECORD")
    private String medicalRecord;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID", unique = true, referencedColumnName = "ID")
    private User user;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAREGIVER_ID", unique = true, referencedColumnName = "ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Caregiver caregiver;

    public Patient() {
    }

    public Patient(Integer id, String name, Timestamp birthDate, String gender, String address, String medicalRecord) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

}












