package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthDate(),
                caregiver.getGender(),
                caregiver.getAddress(),
                caregiver.getUser().getUsername(),
                caregiver.getUser().getPassword());
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
        return new Caregiver(
                caregiverDTO.getId(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthDate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress());
    }
}
