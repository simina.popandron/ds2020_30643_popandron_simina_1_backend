package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.entities.User;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User patient){
        return new UserDTO(
                patient.getId(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getRole());
    }

    public static User generateEntityFromDTO(UserDTO patientDTO){
        return new User(
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getRole());
    }

}









