package ro.tuc.ds2020.dtos;

import java.sql.Timestamp;

public class IntakeDTO {

    private Integer id;
    private Timestamp startDate;
    private Timestamp endDate;
    private Integer medicationId;
    private Integer patientId;

    public IntakeDTO() {
    }

    public IntakeDTO(Integer id, Timestamp startDate, Timestamp endDate) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Integer getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Integer medicationId) {
        this.medicationId = medicationId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

}
