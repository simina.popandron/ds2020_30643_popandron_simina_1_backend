package ro.tuc.ds2020.dtos.validators;

import ro.tuc.ds2020.dtos.PatientDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.ArrayList;
import java.util.List;

public class PatientValidator {

    private static final Log LOGGER = LogFactory.getLog(PatientValidator.class);

    private PatientValidator(){}

    public static void validateInsertOrUpdate(PatientDTO patientDTO) {

        List<String> errors = new ArrayList<>();
        if (patientDTO == null) {
            errors.add("personDTO is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
        }
    }

}
