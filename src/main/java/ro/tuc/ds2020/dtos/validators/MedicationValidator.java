package ro.tuc.ds2020.dtos.validators;

import ro.tuc.ds2020.dtos.MedicationDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.ArrayList;
import java.util.List;

public class MedicationValidator {

    private static final Log LOGGER = LogFactory.getLog(MedicationValidator.class);

    private MedicationValidator(){}

    public static void validateInsertOrUpdate(MedicationDTO medicationDTO) {

        List<String> errors = new ArrayList<>();
        if (medicationDTO == null) {
            errors.add("medicationDTO is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
        }
    }

}
