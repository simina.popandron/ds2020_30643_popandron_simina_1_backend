package ro.tuc.ds2020.dtos.validators;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.ArrayList;
import java.util.List;

public class CaregiverValidator {

    private static final Log LOGGER = LogFactory.getLog(CaregiverValidator.class);

    private CaregiverValidator(){}

    public static void validateInsertOrUpdate(CaregiverDTO caregiverDTO) {

        List<String> errors = new ArrayList<>();
        if (caregiverDTO == null) {
            errors.add("caregiverDTO is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
        }
    }
}
